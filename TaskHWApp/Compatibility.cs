﻿using Newtonsoft.Json;

namespace TaskHWApp
{
    public class Compatibility
    {
        [JsonProperty(PropertyName = "fname")]
        public string FirstPartner { get; set; }
        [JsonProperty(PropertyName = "sname")]
        public string SecondPartner { get; set; }
        [JsonProperty(PropertyName = "percentage")]
        public int Percentage { get; set; }
        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }
    }
}